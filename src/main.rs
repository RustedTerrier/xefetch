use std::{env, fs, path::Path, process::{exit, Command}};

fn main() {
    let os = match env::consts::OS {
        | "linux" => get_distro(),
        | a => a.to_owned()
    };

    let desktop_environment = get_env("DESKTOP_SESSION");
    let shell = get_env("SHELL");
    let username = get_env("USER");
    let hostname = get_hostname();
    let kern_version = get_kernel_version();
    let uptime = get_uptime();
    let model = get_model();
    let cpu = get_cpu();
    let pkgs = get_pkgs().replace("qlist", "portage");

    render(&get_art(&os),
           &os,
           &username,
           &hostname,
           &model,
           &kern_version,
           &uptime,
           &shell,
           &desktop_environment,
           &cpu,
           &pkgs,
           get_color(&os));
}

fn get_distro() -> String {
    if Path::new("/etc/sourcemage-release").exists() {
        return "Source Mage GNU/Linux".to_owned();
    }

    let file = match fs::read_to_string("/etc/os-release") {
        | Ok(a) => a,
        | Err(_) => {
            eprintln!("XEFETCH: no valid /etc/os-release was found. Please create one to use xefetch.");
            exit(1);
        }
    };

    let file_lines: Vec<&str> = file.split('\n').collect();

    for i in file_lines {
        if i.starts_with("NAME=") {
            let distro = i.trim()
                          .trim_start_matches("NAME=")
                          .trim_start_matches('"')
                          .trim_end_matches('"');
            return if !distro.to_ascii_uppercase().contains("LINUX") {
                format!("{} Linux", distro)
            } else {
                i.to_owned()
            };
        }
    }

    "Linux".to_owned()
}

fn get_env(env: &str) -> String {
    // Read the environment variable and return the value after the last '/'.
    // If an error occurs return Unknown
    match env::var(env) {
        | Ok(a) => match a.split('/').collect::<Vec<&str>>().last() {
            | Some(b) => b.to_string(),
            | None => a.to_owned()
        },
        | _ => "Unknown".to_owned()
    }
}

fn get_hostname() -> String {
    match fs::read_to_string("/proc/sys/kernel/hostname") {
        | Ok(a) => return a.trim().to_owned(),
        | _ => ()
    }

    match fs::read_to_string("/etc/hostname") {
        | Ok(a) => a.trim().to_owned(),
        | _ => String::new()
    }
}

fn get_kernel_version() -> String {
    match fs::read_to_string("/proc/sys/kernel/osrelease") {
        | Ok(a) => a.trim().to_owned(),
        | _ => "Unknown".to_owned()
    }
}

fn get_uptime() -> String {
    let uptime_string = match fs::read_to_string("/proc/uptime") {
        | Ok(a) => {
            match a.split('.').collect::<Vec<&str>>().first() {
                | Some(b) => b.to_string(),
                // Should I even add an error message?
                | None => return "Unknown".to_owned()
            }
        },
        | Err(e) => {
            eprintln!("XEFETCH: unable to read /proc/uptime : {}", e);
            exit(1);
        }
    };

    let uptime_num = match uptime_string.parse::<usize>() {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("XEFETCH: uptime: unable to convert {} to u64: {}.",
                      uptime_string, e);
            exit(1);
        }
    };

    // Only add hours or minutes if they are not 0.
    let mut output = String::new();

    if uptime_num >= 86400 {
        output = format!("{} Days", uptime_num / 86400);
    }

    if uptime_num >= 3600 {
        output = format!("{} Hours ", uptime_num / 3600 % 24);
    }

    if uptime_num >= 60 {
        output = format!("{}{} Minutes ", output, uptime_num / 60 % 60);
    }

    format!("{}{} Seconds", output, uptime_num % 60)
}

fn get_model() -> String {
    let name =
        match fs::read_to_string("/sys/devices/virtual/dmi/id/product_name") {
            | Ok(a) => a.trim().to_owned(),
            | Err(e) => {
                eprintln!("XEFETCH: host: unable to read /sys/devices/virtual/dmi/id/product_name: {}.", e);
                "Unknown".to_owned()
            }
        };

    let version =
        match fs::read_to_string("/sys/devices/virtual/dmi/id/product_version")
        {
            | Ok(a) => a.trim().to_owned(),
            | Err(e) => {
                eprintln!("XEFETCH: host: unable to read /sys/devices/virtual/dmi/id/product_version: {}.", e);
                "Unknown".to_owned()
            }
        };

    format!("{} {}", name, version)
}

fn get_cpu() -> String {
    let file = match fs::read_to_string("/proc/cpuinfo") {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("XEFETCH: cpuinfo: unable to read /proc/cpuinfo: {}: unable to find CPU info.", e);
            return "Unknown".to_owned();
        }
    };

    let file_lines: Vec<&str> = file.split('\n').collect();

    for i in file_lines {
        if i.starts_with("model name") {
            match i.split(':').collect::<Vec<&str>>().last() {
                | Some(a) => {
                    return a.trim_start().trim().replace("(R) Core(TM)", "")
                },
                | None => return "Unknown".to_owned()
            }
        }
    }

    "Unknown".to_owned()
}

// the bin name is the binary name while the cmd is the full command, like `xbps-query -l`
fn get_pkg(bin_name: &str, cmd: &str) -> String {
    // The package manager should manage itself and according to the linux hierarchy the binary should then be placed in /usr/bin
    if Path::new(&format!("/usr/bin/{}", bin_name)).exists() {
        let output = match Command::new("/bin/sh").arg("-c").arg(&cmd).output()
        {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("XEFETCH: pkg: unable to spawn /bin/sh -c \"{}\": {}.", bin_name, e);
                exit(1);
            }
        };

        format!("{} ({})",
                String::from_utf8_lossy(&output.stdout).split('\n').count() - 1,
                bin_name)
    } else {
        String::new()
    }
}

fn get_pkgs() -> String {
    let mut pkgs = Vec::new();

    let pkg_spawn = [("apk", "apk info"),
                     ("apt", "apt --installed"),
                     ("dnf", "dnf list installed"),
                     ("flatpak", "flatpak list"),
                     ("gaze", "gaze installed -q"),
                     ("nix-env", "nix-env -qa --installed \"*\""),
                     ("pacman", "pacman -Q -q"),
                     ("qlist", "qlist -l"),
                     ("snap", "snap list"),
                     ("xbps-query", "xbps-query -l"),
                     ("xepkg", "xepkg -l installed"),
                     ("zypper", "zypper se --installed-only")];

    for (i, j) in pkg_spawn {
        let output = get_pkg(i, j);
        if !output.is_empty() {
            pkgs.push(output);
        }
    }

    pkgs.join(", ")
}

fn get_art(os: &str) -> [&'static str; 7] {
    match &os.to_ascii_uppercase()[..] {
        | "VOID LINUX" => ["   dMMMMMMb",
                           " dMMV`  `*VMb",
                           " `V  ,mm.  VMA",
                           "A,  (MMMM)  `V",
                           "VMA  `**'  A,",
                           " PMAm-__-AMMP",
                           "   *MMMMMMP"],
        | "ALPINE LINUX" => ["   AMMMMMMMA",
                             "  AMMVVMM?MMA",
                             " AMMV  VV VMMA",
                             " MMV A  \\  VMM",
                             " VV AM   \\  VV",
                             "  VMMMMMMMMMV",
                             "   VMMMMMMMV"],
        | "SOURCE MAGE GNU/LINUX" => ["     ,mMMA",
                                      "  <=AMMMMMA",
                                      "     AMMMMMm",
                                      "     VMMMMMMA",
                                      "     ,VMMMMMM",
                                      "    ~' MMMMMM",
                                      "       `*MMAV"],
        | "MANJARO LINUX" => ["  MMMMMMMM  MMM",
                              "  MMMMMMMM  MMM",
                              "  MMM\"\"\"\"\"  MMM",
                              "  MMM  MMM  MMM",
                              "  MMM  MMM  MMM",
                              "  MMM  MMM  MMM",
                              "  MMM  MMM  MMM"],
        | "XERUX LINUX" => ["  VMMA    AMMV",
                            "\x1b[0m   VMMA  AMMV    ",
                            "\x1b[0m    VMMAAMMV     ",
                            "\x1b[0m     XMMMMX      ",
                            "\x1b[0m    AMMVVMMA     ",
                            "\x1b[0m   AMMV  VMMA    ",
                            "  AMMV    VMMA"],
        | "ARCH LINUX" => ["       A",
                           "      AMA",
                           "     `*<MA",
                           "    AY>,MMA",
                           "   AMMY`YMMA",
                           "  AMM0   0A>*",
                           " AM-*     *-MA"],
        | "LINUX MINT" => ["MMMMMMMMMMMMo",
                           "M   _       `o",
                           "MMo M  m\"m\"m M",
                           "  M M  M M M M",
                           "  M M  \" \" # M",
                           "  o `\"\"\" **` M",
                           "   *MMMMMMMMMM"],
        | "MX LINUX" => ["       VA  A`",
                         "        V,M`",
                         "      A.,MA",
                         "     AMM* VA",
                         "    AMMMA  VA",
                         "   AMMMMMA.AMA",
                         "  AMMMMMMMMMMMA"],
        | _ => ["      ,aa,",
                "     ;A00A;",
                "     AV\x1b[33m<>\x1b[0;1mVA,     ",
                "    AMV  VMA",
                "    MV    VM,",
                "   \x1b[33mJ\\\x1b[0;1mM;,,;M\x1b[33m/L,\x1b[0;1m   ",
                "   \x1b[33mVMV\x1b[0;1m\"\"\"\"\x1b[33mVMV\x1b[0;1m    "]
    }
}

fn get_color(os: &str) -> &'static str {
    match &os.to_ascii_uppercase()[..] {
        | "VOID LINUX" | "XERUX LINUX" | "MANJARO LINUX" | "LINUX MINT" => {
            "\x1b[32;1m"
        },
        | "ALPINE LINUX" | "ARCH LINUX" => "\x1b[34;1m",
        | "SOURCE MAGE GNU/LINUX" => "\x1b[31;1m",
        | _ => "\x1b[1m"
    }
}

fn render(art: &[&str], os: &str, user: &str, hostname: &str, model: &str,
          kernel_version: &str, uptime: &str, shell: &str, de: &str,
          cpu: &str, pkgs: &str, color: &str) {
    let colors = "\x1b[40m  \x1b[41m  \x1b[42m  \x1b[43m  \x1b[44m  \x1b[45m  \x1b[46m  \x1b[47m  \x1b[0m";
    let light_colors = "\x1b[100m  \x1b[101m  \x1b[102m  \x1b[103m  \x1b[104m  \x1b[105m  \x1b[106m  \x1b[107m  \x1b[0m";
    println!("{}{: <#17}{}\x1b[0m@{}{}", color, art[0], user, color, hostname);
    println!("{}{: <#17}{}OS:\x1b[0m {} {}",
             color,
             art[1],
             color,
             os,
             env::consts::ARCH);
    println!("{}{: <#17}{}HOST:\x1b[0m {}", color, art[2], color, model);
    println!("{}{: <#17}{}KERNEL:\x1b[0m {}",
             color, art[3], color, kernel_version);
    println!("{}{: <#17}{}UPTIME:\x1b[0m {}", color, art[4], color, uptime);
    println!("{}{: <#17}{}SHELL:\x1b[0m {}", color, art[5], color, shell);
    println!("{}{: <#17}{}DE:\x1b[0m {}", color, art[6], color, de);
    println!("{} {}CPU: \x1b[0m{}", colors, color, cpu);
    println!("{} {}PKGS: \x1b[0m{}", light_colors, color, pkgs);
}
